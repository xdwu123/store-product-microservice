package com.example.store.shoppingservice.model.ext.customer;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Customer {
    private Long id;
    private String numberID;
    private String firstName;
    private String lastName;
    private String email;
    private Region region;
    private String state;
}
