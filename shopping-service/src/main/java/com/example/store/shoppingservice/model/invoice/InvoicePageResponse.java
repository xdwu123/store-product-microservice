package com.example.store.shoppingservice.model.invoice;

import com.example.store.shoppingservice.entity.Invoice;
import com.example.store.shoppingservice.model.PageResponse;

import java.util.List;

public class InvoicePageResponse extends PageResponse<Invoice> {

    private static final long serialVersionUID = 3825829548991465041L;

    public InvoicePageResponse(int currentPage, long totalItems, int totalPages, List<Invoice> content) {
        super(currentPage, totalItems, totalPages, content);
    }

    @Override
    public String toString() {
        return new StringBuilder("InvoicePageResponse{")
                .append("invoices=").append(this.getContent())
                .append('}').toString();
    }
}
