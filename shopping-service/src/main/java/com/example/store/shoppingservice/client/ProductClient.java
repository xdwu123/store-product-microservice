package com.example.store.shoppingservice.client;

import com.example.store.shoppingservice.model.ext.product.Product;
import com.example.store.shoppingservice.model.ext.product.ProductUpdateStockRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "product-service")
@RequestMapping(value = "/store/v1/products")
public interface ProductClient {

    @GetMapping("/{productId}")
    ResponseEntity<Product> getById(@PathVariable("productId") Long productId);

    @PutMapping("/{productId}/update-stock")
    ResponseEntity<Product> updateStock(@PathVariable("productId") Long productId,
                                               @RequestBody ProductUpdateStockRequest updateStockRequest);
}
