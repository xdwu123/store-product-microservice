package com.example.store.shoppingservice.model.ext.product;

import lombok.Data;

@Data
public class Product {

    private Long id;
    private String name;
    private String description;
    private Integer stock;
    private Double price;
    private String status;
    private Category category;
}
