package com.example.store.shoppingservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PageResponse<T> implements Serializable {

    private static final long serialVersionUID = -3510696847585804616L;

    private final int currentPage;
    private final long totalItems;
    private final int totalPages;
    private final List<T> content;

    public PageResponse(int currentPage, long totalItems, int totalPages, List<T> content) {
        this.currentPage = currentPage;
        this.totalItems = totalItems;
        this.totalPages = totalPages;
        this.content = content;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public List<T> getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "PageResponse{" +
                "currentPage=" + currentPage +
                ", totalItems=" + totalItems +
                ", totalPages=" + totalPages +
                ", content=" + content +
                '}';
    }
}
