package com.example.store.shoppingservice.service;

import com.example.store.shoppingservice.entity.Invoice;
import com.example.store.shoppingservice.model.invoice.InvoicePageResponse;


public interface InvoiceService {

    InvoicePageResponse getAll(int page, int size);
    Invoice getById(Long Id);
    Invoice getByNumberInvoice(String numberInvoice);
    Invoice createInvoice(Invoice invoice);
    Invoice updateInvoice(Invoice invoice);
    Invoice deleteInvoice(Long invoiceId);

}
