package com.example.store.shoppingservice.rest;

import com.example.store.shoppingservice.entity.Invoice;
import com.example.store.shoppingservice.model.invoice.InvoicePageResponse;
import com.example.store.shoppingservice.service.InvoiceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@Slf4j
@RestController
@RequestMapping("/store/v1/invoices")
public class InvoiceController {

    private InvoiceService invoiceService;

    @Autowired
    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<InvoicePageResponse> getInvoices(
            @RequestParam(name = "page", defaultValue = "0", required = false) int page,
            @RequestParam(name = "size", defaultValue = "50", required = false)  int size) {

        InvoicePageResponse response =  invoiceService.getAll(page, size);
        if (response.getTotalItems() == 0) {
            return  ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(response);

    }

    @GetMapping("/{invoiceId}")
    public ResponseEntity<Invoice> getInvoiceById(@PathVariable(name = "invoiceId") Long invoiceId) {
        return ResponseEntity.ok(invoiceService.getById(invoiceId));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> createInvoice(@Valid @RequestBody Invoice invoice) {

        Invoice savedInvoice =  invoiceService.createInvoice(invoice);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedInvoice.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping("/{invoiceId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Invoice> updateInvoice(@PathVariable(name = "invoiceId") Long invoiceId,
                                                 @Valid @RequestBody Invoice invoice) {

        invoice.setId(invoiceId);

        Invoice updatedInvoice = invoiceService.updateInvoice(invoice);
        return ResponseEntity.ok(updatedInvoice);
    }

    @DeleteMapping("/{invoiceId}")
    public ResponseEntity<?> deleteInvoice(@PathVariable(name = "invoiceId") Long invoiceId) {
        invoiceService.deleteInvoice(invoiceId);

        return ResponseEntity.noContent().build();
    }
}
