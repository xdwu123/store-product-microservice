package com.example.store.shoppingservice.service.impl;

import com.example.store.shoppingservice.client.CustomerClient;
import com.example.store.shoppingservice.client.ProductClient;
import com.example.store.shoppingservice.entity.Invoice;
import com.example.store.shoppingservice.model.exception.DataValidationException;
import com.example.store.shoppingservice.model.exception.ResourceNotFoundException;
import com.example.store.shoppingservice.model.ext.customer.Customer;
import com.example.store.shoppingservice.model.ext.product.Product;
import com.example.store.shoppingservice.model.ext.product.ProductUpdateStockRequest;
import com.example.store.shoppingservice.model.invoice.InvoicePageResponse;
import com.example.store.shoppingservice.repository.InvoiceItemRepository;
import com.example.store.shoppingservice.repository.InvoiceRepository;
import com.example.store.shoppingservice.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
public class InvoiceServiceImpl implements InvoiceService {

    private InvoiceRepository invoiceRepository;

    private InvoiceItemRepository invoiceItemRepository;

    private CustomerClient customerClient;

    private ProductClient productClient;

    @Autowired
    public InvoiceServiceImpl(InvoiceRepository invoiceRepository, InvoiceItemRepository invoiceItemRepository,
                              CustomerClient customerClient, ProductClient productClient) {
        this.invoiceRepository = invoiceRepository;
        this.invoiceItemRepository = invoiceItemRepository;
        this.customerClient = customerClient;
        this.productClient = productClient;
    }

    @Override
    public InvoicePageResponse getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Invoice> invoicePage = invoiceRepository.findAll(pageable);

        return new InvoicePageResponse(invoicePage.getNumber(), invoicePage.getTotalElements(), invoicePage.getTotalPages(), invoicePage.getContent());
    }

    @Override
    public Invoice getById(Long id) {
        Invoice invoiceDB = getInvoiceOrElseThrowNotFoundException(id);

        Customer customer = customerClient.getById(invoiceDB.getCustomerId()).getBody();
        invoiceDB.setCustomer(customer);

        invoiceDB.getItems().forEach(invoiceItem -> {
            Product product = productClient.getById(invoiceItem.getProductId()).getBody();
            invoiceItem.setProduct(product);
        });

        return invoiceDB;
    }

    @Override
    public Invoice getByNumberInvoice(String numberInvoice) {
        return invoiceRepository.findByNumberInvoice(numberInvoice)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Invoice Number: {0} does not exists", numberInvoice)));
    }

    @Override
    public Invoice createInvoice(Invoice invoice) {

          if(invoiceRepository.findByNumberInvoice(invoice.getNumberInvoice()).isPresent()) {
              throw  new DataValidationException(String.format("Number Invoice: {0} already exists"));
          }

          invoice.setState("CREATED");

          Invoice newInvoice = invoiceRepository.save(invoice);
          newInvoice.getItems().forEach( invoiceItem -> {

              ProductUpdateStockRequest updateStockRequest = new ProductUpdateStockRequest();
              updateStockRequest.setProductId(invoiceItem.getProductId());
              updateStockRequest.setQuantity(invoiceItem.getQuantity() * -1);

              productClient.updateStock(invoiceItem.getProductId(), updateStockRequest);

          });

          return newInvoice;

    }

    @Override
    public Invoice updateInvoice(Invoice invoice) {
        Invoice invoiceDB = getInvoiceOrElseThrowNotFoundException(invoice.getId());

        invoiceDB.setCustomerId(invoice.getCustomerId());
        invoiceDB.setDescription(invoice.getDescription());
        invoiceDB.setNumberInvoice(invoice.getNumberInvoice());
        invoiceDB.getItems().clear();
        invoiceDB.setItems(invoice.getItems());

        return invoiceRepository.save(invoiceDB);
    }

    @Override
    public Invoice deleteInvoice(Long invoiceId) {
        Invoice invoiceDB = getInvoiceOrElseThrowNotFoundException(invoiceId);

        invoiceDB.setState("DELETED");
        return invoiceRepository.save(invoiceDB);
    }

    public Invoice getInvoiceOrElseThrowNotFoundException (final Long id) {
        return this.invoiceRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("InvoiceId: %d does not exists", id)));
    }
}
