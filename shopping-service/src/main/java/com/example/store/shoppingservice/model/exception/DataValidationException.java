package com.example.store.shoppingservice.model.exception;

public class DataValidationException extends RuntimeException {

    private static final long serialVersionUID = -4027424334186478043L;

    public DataValidationException(String message) {
        super(message);
    }
}
