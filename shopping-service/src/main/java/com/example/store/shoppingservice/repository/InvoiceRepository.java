package com.example.store.shoppingservice.repository;

import com.example.store.shoppingservice.entity.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    public List<Invoice> findByCustomerId(Long customerId);
    public Optional<Invoice> findByNumberInvoice(String numberInvoice);
}
