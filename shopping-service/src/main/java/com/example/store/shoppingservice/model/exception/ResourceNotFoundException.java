package com.example.store.shoppingservice.model.exception;

public class ResourceNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 4662897619585911271L;

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
