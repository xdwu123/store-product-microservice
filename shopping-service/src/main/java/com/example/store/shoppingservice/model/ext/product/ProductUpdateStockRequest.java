package com.example.store.shoppingservice.model.ext.product;

import lombok.Data;

@Data
public class ProductUpdateStockRequest {
    private Long productId;
    private Integer quantity;
}
