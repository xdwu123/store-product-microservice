package com.example.store.product.service.impl;

import com.example.store.product.entity.Category;
import com.example.store.product.entity.Product;
import com.example.store.product.model.exception.ResourceNotFoundException;
import com.example.store.product.model.product.PageProductResponse;
import com.example.store.product.repository.ProductRepository;
import com.example.store.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public PageProductResponse listAll(Optional<String> status, int page, int size) {

        Pageable pageable = PageRequest.of(page, size);
        Page<Product> pageProduct;
        if (status.isPresent()) {
            pageProduct = this.productRepository.findByStatus(status.get().toUpperCase(), pageable);
        } else {
            pageProduct = this.productRepository.findAll(pageable);
        }

        PageProductResponse pageProductResponse = new PageProductResponse(pageProduct.getNumber(), pageProduct.getTotalElements(),
                pageProduct.getTotalPages(), pageProduct.getContent());

        return pageProductResponse;
    }

    @Override
    public Product getById(Long id) {
        return getProductOrElseThrowNotFoundException(id);
    }

    @Override
    public Product create(Product product) {
        product.setStatus("CREATED");
        product.setCreateAt(new Date());

        return this.productRepository.save(product);
    }

    @Override
    public Product update(Product product) {
        Product productForUpdate = getProductOrElseThrowNotFoundException(product.getId());

        productForUpdate.setName(product.getName());
        productForUpdate.setDescription(product.getDescription());
        productForUpdate.setCategory(product.getCategory());
        productForUpdate.setPrice(product.getPrice());

        return this.productRepository.save(productForUpdate);
    }

    @Override
    public Product deleteById(Long id) {
        Product productForDelete = getProductOrElseThrowNotFoundException(id);


        productForDelete.setStatus("DELETED");
        return this.productRepository.save(productForDelete);
    }

    @Override
    public List<Product> findByCategory(Category category) {
        return this.productRepository.findByCategory(category);
    }

    @Override
    public Product updateStock(Long id, Integer quantity) {
        Product productForUpdate = getProductOrElseThrowNotFoundException(id);

        productForUpdate.setStock(productForUpdate.getStock() + quantity);

        return this.productRepository.save(productForUpdate);
    }

    private Product getProductOrElseThrowNotFoundException(final Long id) {
        return this.productRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("ProductId: %d does not exists.", id)));
    }
}
