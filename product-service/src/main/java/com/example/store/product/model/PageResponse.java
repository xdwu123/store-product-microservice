package com.example.store.product.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PageResponse<T> implements Serializable {
    private static final long serialVersionUID = 562286439520453875L;

    private int currentPage;
    private long totalItems;
    private int totalPages;
    private List<T> data;

    @Override
    public String toString() {
        return new StringBuilder("PageResponse{")
                .append("currentPage=").append(currentPage)
                .append(", totalItems=").append(totalItems)
                .append(", totalPages=").append(totalPages)
                .append(", data=").append(data)
                .append('}').toString();
    }
}
