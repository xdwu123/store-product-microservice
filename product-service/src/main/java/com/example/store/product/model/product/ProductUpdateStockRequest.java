package com.example.store.product.model.product;

import java.io.Serializable;

public class ProductUpdateStockRequest implements Serializable {

    private static final long serialVersionUID = 3889939990556855582L;

    private Long productId;
    private Integer quantity;

    public ProductUpdateStockRequest() {
    }

    public ProductUpdateStockRequest(Long productId, Integer quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
