package com.example.store.product.model.product;

import com.example.store.product.entity.Product;
import com.example.store.product.model.PageResponse;
import lombok.Data;

import java.util.List;

@Data
public class PageProductResponse extends PageResponse<Product> {

    private static final long serialVersionUID = -8499649711831886151L;

    public PageProductResponse(int currentPage, long totalItems, int totalPages, List<Product> products) {
        super(currentPage, totalItems, totalPages, products);
    }

    @Override
    public String toString() {
        return new StringBuilder("PageProductResponse{")
                .append("products=").append(this.getData())
                .append('}').toString();
    }
}
