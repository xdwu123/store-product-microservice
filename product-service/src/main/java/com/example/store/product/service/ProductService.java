package com.example.store.product.service;

import com.example.store.product.entity.Category;
import com.example.store.product.entity.Product;
import com.example.store.product.model.product.PageProductResponse;


import java.util.List;
import java.util.Optional;

public interface ProductService {

    PageProductResponse listAll(Optional<String> status, int page, int size);

    Product getById(Long id);

    Product create(Product product);

    Product update(Product product);

    Product deleteById(Long Id);

    List<Product> findByCategory(Category category);

    Product updateStock(Long id, Integer quantity);

}
