package com.example.store.customer.service.impl;

import com.example.store.customer.entity.Customer;
import com.example.store.customer.entity.Region;
import com.example.store.customer.model.customer.PageCustomerResponse;
import com.example.store.customer.model.exception.DataValidationException;
import com.example.store.customer.model.exception.ResourceNotFoundException;
import com.example.store.customer.repo.CustomerRepository;
import com.example.store.customer.service.CustomerService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;


@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer create(Customer customer) {
        Optional<Customer> customerDB = customerRepository.findByNumberID(customer.numberID);
        if (customerDB.isPresent()) {
            throw new DataValidationException(String.format("NumberId: %s already exists", customer.numberID));
        }

        customer.setState("CREATED");
        customer.setCreateAt(new Date());
        return customerRepository.save(customer);
    }

    @Override
    public Customer update(Customer customer) {
        Customer customerDB = getCustomerOrElseThrowNotFoundException(customer.getId());
        customerDB.setFirstName(customer.getFirstName());
        customerDB.setLastName(customer.getLastName());
        customerDB.setEmail(customer.getEmail());
        return customerRepository.save(customerDB);
    }

    @Override
    public Customer getById(Long id) {
        return getCustomerOrElseThrowNotFoundException(id);
    }

    @Override
    public PageCustomerResponse listAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Customer> pageCustomer = customerRepository.findAll(pageable);
        PageCustomerResponse pageCustomerResponse = new PageCustomerResponse(pageCustomer.getNumber(),
                pageCustomer.getTotalElements(), pageCustomer.getTotalPages(), pageCustomer.getContent());

        return pageCustomerResponse;
    }

    @Override
    public PageCustomerResponse findByRegion(Region region, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Customer> pageCustomer = customerRepository.findByRegion(region, pageable);

        PageCustomerResponse pageCustomerResponse = new PageCustomerResponse(pageCustomer.getNumber(),
                pageCustomer.getTotalElements(), pageCustomer.getTotalPages(), pageCustomer.getContent());

        return pageCustomerResponse;
    }

    @Override
    public Customer deleteById(Long id) {
        Customer customerDB = getCustomerOrElseThrowNotFoundException(id);
        customerDB.setState("DELETED");
        return customerRepository.save(customerDB);
    }

    private Customer getCustomerOrElseThrowNotFoundException(final Long Id) {
        return this.customerRepository.findById(Id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("CustomerId: %d does not exists", Id)));
    }
}
