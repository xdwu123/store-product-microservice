package com.example.store.customer.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PageResponse<T> implements Serializable {

    private static final long serialVersionUID = 7071336443955248550L;


    private int currentPage;
    private long totalItems;
    private int totalPages;
    private List<T> content;

    public PageResponse(int currentPage, long totalItems, int totalPages, List<T> content) {
        this.currentPage = currentPage;
        this.totalItems = totalItems;
        this.totalPages = totalPages;
        this.content = content;
    }


    @Override
    public String toString() {
        return "PageResponse{" +
                "currentPage=" + currentPage +
                ", totalItems=" + totalItems +
                ", totalPages=" + totalPages +
                ", content=" + content +
                '}';
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public List<T> getContent() {
        return content;
    }
}
