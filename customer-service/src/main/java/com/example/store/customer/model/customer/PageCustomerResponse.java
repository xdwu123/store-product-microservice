package com.example.store.customer.model.customer;

import com.example.store.customer.entity.Customer;
import com.example.store.customer.model.PageResponse;

import java.util.List;

public class PageCustomerResponse extends PageResponse<Customer> {

    private static final long serialVersionUID = 7359884981214424811L;

    public PageCustomerResponse(int currentPage, long totalItems, int totalPages, List<Customer> content) {
        super(currentPage, totalItems, totalPages, content);
    }

    @Override
    public String toString() {
        return new StringBuilder("PageCustomerResponse{")
                .append("customers=").append(this.getContent())
                .append('}').toString();
    }
}
