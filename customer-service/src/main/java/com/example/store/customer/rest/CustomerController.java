package com.example.store.customer.rest;

import com.example.store.customer.entity.Customer;
import com.example.store.customer.model.customer.PageCustomerResponse;
import com.example.store.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/store/v1/customers")
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<PageCustomerResponse> getAll(
            @RequestParam(name = "size", defaultValue = "50", required = false) int size,
            @RequestParam(name = "page", defaultValue = "0", required = false) int page) {

        PageCustomerResponse customers = customerService.listAll(page, size);
        return ResponseEntity.ok(customers);
    }

    @GetMapping("/{customerId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Customer> getById(@PathVariable(name = "customerId") Long customerId) {
        return ResponseEntity.ok(customerService.getById(customerId));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> create(@RequestBody @Valid Customer customer) {
        Customer customerAdded = customerService.create(customer);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(customerAdded.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @DeleteMapping("/{customerId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<?> deleteById(@PathVariable("customerId") Long customerId) {
        customerService.deleteById(customerId);

        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{customerId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Customer> update(@PathVariable(name = "customerId") Long customerId, @RequestBody @Valid Customer customer) {
        customer.setId(customerId);

        Customer customerUpdated = customerService.update(customer);

        return ResponseEntity.ok(customerUpdated);
    }
}
