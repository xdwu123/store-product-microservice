package com.example.store.customer.model.exception;

public class DataValidationException extends RuntimeException {
    private static final long serialVersionUID = -8633515537458875839L;

    public DataValidationException(String message) {
        super(message);
    }
}
